package com.sda.vendingmachine.service.exceptions;

public class NotSufficientChangeException extends Exception {

    private String insufficientMoneyForChange;

    public NotSufficientChangeException(String insufficientMoneyForChange) {
        this.insufficientMoneyForChange = insufficientMoneyForChange;
    }

    public String getInsufficientMoneyForChange() {
        return insufficientMoneyForChange;
    }
}
