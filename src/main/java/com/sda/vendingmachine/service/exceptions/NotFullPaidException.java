package com.sda.vendingmachine.service.exceptions;

public class NotFullPaidException extends Exception{

    private String notEnoughMoneyForThisProduct;
    private float difference;

    public NotFullPaidException(String notEnoughMoneyForThisProduct, float difference) {
        this.notEnoughMoneyForThisProduct = notEnoughMoneyForThisProduct;
        this.difference = difference;
    }

    public float getDifference() {
        return difference;
    }

    public String getNotEnoughMoneyForThisProduct() {
        return notEnoughMoneyForThisProduct + difference;
    }
}
