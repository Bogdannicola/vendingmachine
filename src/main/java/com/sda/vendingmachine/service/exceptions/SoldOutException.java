package com.sda.vendingmachine.service.exceptions;

public class SoldOutException extends Exception {

    private String outOfStoc;

    public SoldOutException(String outOfStoc) {
        this.outOfStoc = outOfStoc;
    }

    public String getOutOfStoc() {
        return outOfStoc;
    }
}
