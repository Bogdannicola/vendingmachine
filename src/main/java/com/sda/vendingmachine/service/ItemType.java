package com.sda.vendingmachine.service;

public enum ItemType {

    CHOCOLATE, SODA, SNACK
}
