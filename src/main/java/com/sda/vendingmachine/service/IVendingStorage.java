package com.sda.vendingmachine.service;

import com.sda.vendingmachine.persistence.entities.Item;
import com.sda.vendingmachine.service.exceptions.NotFullPaidException;
import com.sda.vendingmachine.service.exceptions.NotSufficientChangeException;

public interface IVendingStorage {

    public boolean checkInsertedAmount(ItemType itemType) throws NotFullPaidException;

    public boolean checkForChange() throws NotSufficientChangeException;

    public Item releaseItem(Item selectedItem);

    public void addMoney(int value);

}
